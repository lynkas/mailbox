module Mailbox

go 1.14

require github.com/gin-gonic/gin v1.6.2

require github.com/imroc/req v0.3.0

require github.com/sendgrid/sendgrid-go v3.6.0+incompatible
