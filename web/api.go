package web

import (
	"Mailbox/core"
	"Mailbox/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"
)

var Config struct {
	TimeZone string `json:"time_zone"`
	Port int `json:"port"`
	Prefix string `json:"prefix"`
	TempFolder string `json:"temp_folder"`
}

var CONF = "config.json"

func init() {
	data, _ := ioutil.ReadFile(CONF)
	err := json.Unmarshal(data, &Config)
	if err!=nil {
		panic(err)
	}
	utils.MakeDir(Config.TempFolder)
}

func ApiInit()  {
	r := gin.Default()
	//r.Use(RequestLogger())
	r.POST("/inbound", in)
	r.GET("/one", one)
	g:=r.Group("/"+Config.Prefix)
	g.GET("/ping", pass)
	g.GET("/month", month)
	g.GET("/months", months)

	r.Run(fmt.Sprintf(":%d",Config.Port)) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}

func pass(c *gin.Context) {
	c.JSON(http.StatusOK,gin.H{})
}

func one(c *gin.Context) {
	month := c.DefaultQuery("month", "")
	token := c.DefaultQuery("token", "")
	mail,err:=core.Query(month,token)
	if err!=nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}
	c.JSON(http.StatusOK,mail)
}

func month(c *gin.Context) {
	month := c.DefaultQuery("month", "")
	mhs,err:=core.ListOneMonth(month)
	if err!=nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}
	c.JSON(http.StatusOK,mhs)
}

func months(c *gin.Context) {
	mhs:=core.ListMonths()
	c.JSON(http.StatusOK,mhs)
}

func in(c *gin.Context){
	var data Data
	err:=c.ShouldBind(data)
	var formDate map[string]string
	err = c.ShouldBind(&formDate)

	if err!=nil {
		log.Println(",,,")
		c.JSON(http.StatusBadRequest,gin.H{})
		return
	}

	id :=utils.Random(128)
	utils.MakeDir(path.Join(Config.TempFolder,id))
	var appendix []*core.Appendix
	var att []string
	p:=path.Join(Config.TempFolder, id)
	utils.MakeDir(p)
	attachmentNumber,err := strconv.Atoi(c.DefaultPostForm("attachments","0"))

	for i:=1;i<=attachmentNumber;i++ {
		file,err:=c.FormFile("attachment"+strconv.Itoa(i))
		if err!=nil {
			log.Println(err)
			continue
		}
		p=path.Join(Config.TempFolder, id,file.Filename)
		err=c.SaveUploadedFile(file,p)
		for os.IsExist(err) {
			p=path.Join(Config.TempFolder, id,utils.Random(3)+file.Filename)
			err=c.SaveUploadedFile(file,p)
		}
		att=append(att, p)
	}

	appendix,_=core.SaveAppendix(att...)



	os.RemoveAll(path.Join(Config.TempFolder,id))
	//if len(parsedEmail.Attachments)>0 {
	//	var att []string
	//	for filename, contents := range parsedEmail.Attachments {
	//		fo,err:=os.Create(path.Join(Config.TempFolder,id,filename))
	//		for os.IsExist(err) {
	//			filename=utils.Random(1)+filename
	//			fo,err=os.Create(path.Join(Config.TempFolder,id,filename))
	//		}
	//		b,_:=base64.StdEncoding.DecodeString(string(contents))
	//		_,err=fo.Write(b)
	//
	//		att=append(att, path.Join(Config.TempFolder,id,filename))
	//		fo.Close()
	//	}
	//
	//	appendix,_=core.SaveAppendix(att...)
	//	os.RemoveAll(path.Join(Config.TempFolder,id))
	//}
	mail:=core.Mail{
		From:        c.PostForm("from"),
		To:          c.PostForm("to"),
		Subject:     c.PostForm("subject"),
		TextContent: c.PostForm("text"),
		HTMLContent: c.PostForm("html"),
		Time:        time.Now(),
		Appendix:    appendix,
	}

	//for section, body := range parsedEmail.Body {
	//	if strings.Contains(section,"html") {
	//		mail.HTMLContent = body
	//		continue
	//	}
	//	if strings.Contains(section,"plain") {
	//		mail.TextContent = body
	//		continue
	//	}
	//}
	c.JSON(http.StatusOK,gin.H{})
	err=mail.Save(callback)
	return
}

type Data struct {
	Subject        string
	To             string
	From           string
	Text           string
	Html           string
	AttachmentInfo map[string]struct{
		Filename string
		Name string
		Type string
	}

}


func RequestLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		buf, _ := ioutil.ReadAll(c.Request.Body)
		rdr1 := ioutil.NopCloser(bytes.NewBuffer(buf))
		rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf)) //We have to create a new Buffer, because rdr1 will be read.

		fmt.Println(readBody(rdr1)) // Print request body

		c.Request.Body = rdr2
		c.Next()
	}
}

func readBody(reader io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)

	s := buf.String()
	return s
}