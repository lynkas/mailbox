package core

import (
	"sort"
)

var index map[string]*map[string]int
var db []*Mail

func Query(month string, token string) (*Mail,error) {
	thisMonth,ok:=index[month]
	if !ok {
		return nil, NewNotFoundError()
	}
	id,ok:=(*thisMonth)[token]
	if !ok {
		return nil, NewNotFoundError()
	}
	return db[id],nil
}

func Add(mail *Mail,month string,token string) error {
	id:=len(db)
	thisMonth,ok:=index[month]
	if !ok {
		index[month]=&map[string]int{}
	}
	thisMonth,_=index[month]
	_,ok=(*thisMonth)[token]
	if ok {
		return NewExistError()
	}else {
		(*thisMonth)[token]=id
	}
	db=append(db, mail)
	return nil
}

func ListMonths() []string  {
	keys := make([]string, 0, len(index))
	for k := range index {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.StringSlice(keys)))

	return keys
}

func ListOneMonth(month string) ([]*MailHeadline,error) {
	thisMonth,ok:=index[month]
	if !ok {
		return nil, NewNotFoundError()
	}

	var ter []*MailHeadline

	for k,v:=range *thisMonth{

		ter=append(ter,&MailHeadline{
			Token:   k,
			Month:   month,
			Subject: db[v].Subject,
			From:    db[v].From,
			Time:    db[v].Time,
		})
	}

	sort.Slice(ter, func(i, j int) bool {
		return ter[i].Time.Unix()>ter[j].Time.Unix()
	})

	return ter,nil
}