package core

import "errors"

type NotFoundError struct {

}

func (*NotFoundError)Error() string {
	return "file not found"
}

func NewNotFoundError() error {
	return &NotFoundError{}
}
type ExistError struct {

}

func (*ExistError)Error() string {
	return "file exists"
}

func NewExistError() error {
	return &ExistError{}
}
type PathNotValidError struct {

}

func (*PathNotValidError)Error() string {
	return "file path is not valid"
}

func NewPathNotValidError() error {
	return &PathNotValidError{}
}

func IsExistErr(err error) bool {
	return errors.Is(err, NewExistError())
}