package core

import (
	"Mailbox/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type Mail struct {
	From string
	To string
	Subject string
	TextContent string
	HTMLContent string
	Time time.Time
	Appendix []*Appendix
}

type MailHeadline struct {
	Token string
	Month string
	Subject string
	From string
	Time time.Time

}



type Appendix struct {
	Month string
	ID string
	Name string
}

var Config struct{
	TimeZone string `json:"time_zone"`
	ArchiveFolder string `json:"archive_folder"`
	AppendixFolder string `json:"appendix_folder"`
}
var CONF="config.json"

func NoCallback(string,string,*Mail) error {
	return nil
}

func (m *Mail) Save(callback func(string ,string,*Mail)error) (err error) {
	month:=utils.MonthToString(Config.TimeZone)
	token:=utils.Random(128)
	archive,err:=utils.Ensure(path.Join(Config.ArchiveFolder,month),token)
	for os.IsExist(err) {
		token=utils.Random(128)
		archive,err=utils.Ensure(path.Join(Config.ArchiveFolder,month),token)
	}
	if err!=nil {
		return err
	}
	defer archive.Close()
	err=m.DumpTo(archive)
	if err!=nil {
		return err
	}
	err=Add(m,month,token)
	if err!=nil {
		return err
	}

	err=callback(month,token,m)
	return err
}
/**
	path: full path
 */
func (m *Mail)DumpTo(file *os.File) error {
	mByte, _ := json.Marshal(m)
	_,err :=file.Write(mByte)

	return err
}

func SaveAppendix(filepaths ...string) ([]*Appendix,[]error) {
	var ret []*Appendix
	var reterr []error
	month:=utils.MonthToString(Config.TimeZone)
	id:=utils.Random(128)
	p:=path.Join(Config.AppendixFolder,month,id)
	utils.MakeDir(p)
	for _, f :=range filepaths{
		if file, err := os.Stat(f); err==nil{
			name:=file.Name()
			err=os.Rename(f,path.Join(p,name))
			if err!=nil {
				ret=append(ret, nil)
				reterr=append(reterr,err)
				continue
			}
			ret = append(ret,&Appendix{
				Month: month,
				ID:    id,
				Name:  name,
			})
			reterr=append(reterr,nil)

		}else {
			ret=append(ret,nil)
			reterr=append(reterr,err)
			continue
		}
	}

	return ret,reterr

}

func init() {
	//Read Config
	data, _ := ioutil.ReadFile(CONF)
	err := json.Unmarshal(data, &Config)
	if err!=nil {
		panic(err)
	}


	utils.MakeDir(Config.ArchiveFolder)
	utils.MakeDir(Config.AppendixFolder)
	index= map[string]*map[string]int{}
	db=[]*Mail{}
	cnt :=0
	filepath.Walk(Config.ArchiveFolder, func(filespath string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}

		if info.IsDir() {
			return nil
		}
		pathSeg:=strings.Split(filespath,string(os.PathSeparator))
		if len(pathSeg)<2 {
			return NewPathNotValidError()
		}
		var mail Mail
		data, _ := ioutil.ReadFile(filespath)
		err = json.Unmarshal(data, &mail)
		if err != nil {
			return nil
		}
		err=Add(&mail,pathSeg[len(pathSeg)-2],pathSeg[len(pathSeg)-1])
		cnt++
		return nil
	})
	log.Println(fmt.Sprintf("%d",cnt)+" mails loaded")


}

