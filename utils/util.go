package utils

import (
	"fmt"
	"math/rand"
	"os"
	"path"
	"strconv"
	"time"
)

var chars = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

var PERM= os.FileMode(0755)
var PERMFile= os.FileMode(0644)

func Random(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = chars[rand.Intn(len(chars))]
	}
	return string(b)
}

func MonthToString(location string) string {
	t:=LocalTime(location)
	m := ""
	m+=strconv.Itoa(t.Year())
	month:=t.Month()

	m+=fmt.Sprintf("%02d",month)

	return m
}

func LocalTime(location string)(time.Time){
	t:=time.Now()
	loc, err := time.LoadLocation(location)
	if err != nil {
		panic(err)
	}
	t = t.In(loc)
	return t
}

func MakeDir(path string)  {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path,PERM)
	}
}

func Ensure(folder string, filename string) (file *os.File,err error) {
	MakeDir(folder)
	if _, err := os.Stat(path.Join(folder,filename)); os.IsNotExist(err) {
		file,err :=os.OpenFile(path.Join(folder,filename), os.O_WRONLY|os.O_CREATE, PERMFile)
		if err!=nil {
			if file!=nil {
				defer file.Close()
			}
			return nil,err
		}
		return file,nil
	}else if err==nil {
			defer file.Close()
			return nil, os.ErrExist
	}else {
			return nil,err
	}

}

